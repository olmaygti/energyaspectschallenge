(function (eaBank) {
    "use strict";

    eaBank.config(['$stateProvider', '$urlRouterProvider', 'APP_URLS', 'TEMPLATE_URLS',
        function ($stateProvider, $urlRouterProvider, APP_URLS, TEMPLATE_URLS) {
            $stateProvider
                .state('login', {
                    url: APP_URLS.LOGIN,
                    templateUrl: TEMPLATE_URLS.LOGIN,
                    controller: 'LoginCtrl'
                })
                .state('admin', {
                    url: APP_URLS.ADMIN,
                    templateUrl: TEMPLATE_URLS.ADMIN,
                    controller: 'AdminCtrl'
                })
                .state('customer', {
                    url: APP_URLS.CUSTOMER,
                    templateUrl: TEMPLATE_URLS.CUSTOMER,
                    controller: 'CustomerCtrl'
                })
                $urlRouterProvider.when('', APP_URLS.LOGIN);
                $urlRouterProvider.otherwise('', APP_URLS.LOGIN);
            ;
        }
    ]);
}(angular.module('eaBank')));