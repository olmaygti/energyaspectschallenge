(function (eaBankServices) {
    eaBankServices.factory('LogService',['$rootScope', '$timeout', 'I18nService',
        function ($rootScope, $timeout, I18nService) {
            $rootScope.alerts = $rootScope.alerts || [];

            $rootScope.closeAlert = function(index) {
                $rootScope.alerts.splice(index, 1);
            };

            function pushMessage(msg, type, ttl) {
                $rootScope.alerts.push({
                    type: type,
                    msg: msg
                });
                $timeout(function() {
                    $rootScope.closeAlert();
                }, ttl || 1500);
            }

            return {
                log: function (message, ttl) {
                    pushMessage(I18nService.getValue(message), 'warning', ttl);
                },
                error: function (message, ttl) {
                    pushMessage(I18nService.getValue(message), 'danger', ttl);
                },
                success: function (message, ttl) {
                    pushMessage(I18nService.getValue(message), 'success', ttl);
                }

            }
        }
    ])
})(angular.module('eaBank.services'));
