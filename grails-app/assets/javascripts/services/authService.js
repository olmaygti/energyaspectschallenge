(function (eabankServices) {
    eabankServices.factory('AuthService',['$q','$http', '$rootScope', 'User', 'authHeaders', 'API_URLS',
        function ($q, $http, $rootScope, User, authHeaders, URLS) {

            function storeUser(user) {
                // User is a undetailed object returned by the spring-security-rest-plugin
                // Let's fetch the real one
                return User.query({username:user.username}).then(function (data) {
                    // Usernames are unique's, so this should have just one item
                    $rootScope.user = _.extend(
                        new User(data[0]),
                        {access_token: user.access_token}
                    );
                    if (!_(localStorage).isUndefined()) {
                        localStorage.user = JSON.stringify($rootScope.user);
                    }
                    return $rootScope.user;
                });
            }

            function setHeadersForUser(user) {
                if (user) {
                    // From this point on, all requests will send the Authorization
                    // header so access to protected resources is granted.
                    authHeaders['Authorization'] = 'Bearer ' + user.access_token;
                } else if (authHeaders['Authorization']) {
                    delete authHeaders['Authorization'];
                }
            }

            function getUserFromStorage() {
                var stringUser;
                if (!_(localStorage).isUndefined()) {
                    stringUser = localStorage.user;
                    if (!_(stringUser).isUndefined()) {
                        return $rootScope.user = new User(JSON.parse(stringUser));
                    }
                }
            }

            return {
                logout: function() {
                    // Delete user from local Storage and clean the HTTP headers
                    delete authHeaders['Authorization'];
                    localStorage.removeItem('user');
                    delete $rootScope.user;

                },
                login: function (user,pass) {
                    var self = this,
                        deferred = $q.defer();

                    $http.post(URLS.LOGIN_CHALLENGE, {
                        username: user,
                        password: pass,
                    }).success(function (data, status) {
                        setHeadersForUser(data);
                        deferred.resolve(storeUser(data));
                    }).error(function (data,status) {
                        console.log("ERROR: ", data, status);
                        deferred.reject();
                    });
                    return deferred.promise;
                },
                // Returns a promise that will be resolved if the user exists and is valid
                // and rejected if not
                isLoggedIn: function () {
                    var user,
                        self = this,
                        deferred = $q.defer();

                    if (_($rootScope.user).isUndefined()) {
                        //It might be in the localStorage
                        user = getUserFromStorage();
                        if (!_(user).isUndefined()) {
                            $rootScope.user = user;
                            setHeadersForUser(user);
                            deferred.resolve(user);
                        } else {
                            deferred.reject();
                        }
                    } else {
                        deferred.resolve($rootScope.user);
                    }
                    return deferred.promise;
                },
                refreshUser: function () {
                    storeUser($rootScope.user);
                }
            }
        }
    ])
}(angular.module('eaBank.services')));