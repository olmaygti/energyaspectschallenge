(function (eaBankServices) {
    eaBankServices.constant('languages', {
        en: {
            global: {
                username: "Username",
                password: "Password",
                login: "Login",
                logout: "Logout",
                remove: "Remove",
                date: "Date",
                type: "Type",
                amount: "Amount",
                info: "INFO",
                enabled: "enabled",
                disabled: "disabled",
                header: "Welcome to EABank online services"
            },
            validation: {
                notNull: 'This field cannot be null',
                noSpaces: 'This field cannot contain spaces',
                passwordLength: 'Must be at least 6 characters',
                passwordContent: 'Must containt lowercase, upercase and numbers'
            },
            accounts: {
                currency: "Currency",
                balance: "Balance",
                initialBalance: 'Initial Balance',
                iban: "IBAN",
                overdraft: "Overdraft"
            },

            admin: {
                usersList:"User's List",
                id: "ID",
                username:"Username",
                name:"Name",
                surname:"Surname",
                password:"Password",
                newUser: "New User",
                newAccount: "New Account",
                role: "Role",
                chooseRole: "Choose Role",

                messages: {
                    userSaved: 'User succesfully saved',
                    userDeleted: 'User succesfully deleted'
                }
            },
            customer: {
                accountList: "List of accounts",
                deposit: "New Deposit",
                withdraw: "New withdrawal",
                showTransactions: "Show Transactions",
                operate: "Operate"
            }
        }
    })
    .factory('I18nService', ['$interpolate', 'languages', function ($interpolate, languages) {

            function attributeNavigate(object, attribute) {
                if (object && attribute) {
                    var attributeChain = attribute.split('.'),
                        lastAttribute = attributeChain.pop(),
                        current = object;

                    _.each(attributeChain, function (currentAttribute) {
                        if (currentAttribute && current) {
                            current = current[currentAttribute];
                        }
                    });

                    if (current) {
                        return current[lastAttribute];
                    }
                }
                return undefined;
            };

            function getLanguage () {
                var found;
                for(var i=0; i < navigator.languages.length; i++ ) {
                    if((found = languages[navigator.languages[i]] && navigator.languages[i]))
                        break;
                }
                return found
            }

            return {
                getLanguagesMap: function (lang) {
                    var language = lang || getLanguage();

                    if (_(language).isUndefined()) {
                        language = languages.def;
                    }
                    return languages[language];
                },
                getValue: function(key, scope) {
                    var literal = attributeNavigate(this.getLanguagesMap(), key) || key;

                    if(!_(scope).isUndefined()) {
                        literal = $interpolate(literal)(scope);
                    }

                    return literal;
                }
            }
        }]);

}(angular.module('eaBank.services')));