// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better 
// to create separate JavaScript files as needed.
//
//= require jquery
//= require ../bower/lodash/dist/lodash.js
//= require ../bower/angular/angular.js
//= require ../bower/angular-animate/angular-animate.js

//= require ../bower/angular-ui-router/release/angular-ui-router.js
//= require ../bower/angular-ui-bootstrap/src/dropdown/dropdown.js
//= require ../bower/angular-ui-bootstrap/src/position/position.js
//= require ../bower/angular-ui-bootstrap/src/alert/alert.js

//= require lib/awesome-resource.js
//= require services/services.js
//= require services/i18nService.js
//= require services/authService.js
//= require services/logService.js
//= require configuration.js
//= require interceptors/interceptors.js
//= require directives/directives.js
//= require models/models.js
//= require controllers/controllers.js
//= require controllers/adminController.js
//= require controllers/customerController.js
//= require eaBank.js
//= require urlMappings.js

