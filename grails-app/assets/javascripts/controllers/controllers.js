(function () {
    "use strict";
    angular.module('eaBank.controllers', [])
        .controller('LoginCtrl', [
        '$scope',
        '$state',
            '$rootScope',
            'AuthService',
            function ($scope, $state, $rootScope, AuthService) {
                $scope.login = function () {
                    AuthService.login($scope.userData.username, $scope.userData.password)
                        .then(function () {
                            $state.transitionTo($rootScope.user.getRole());
                        })
                        .catch(function () {
                            console.log(':(');
                        });
                };
            }
        ]);
}());