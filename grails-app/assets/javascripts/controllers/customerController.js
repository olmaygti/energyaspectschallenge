(function (eaBankControllers) {
    "use strict";
    eaBankControllers.controller('CustomerCtrl', [
        '$scope',
        '$q',
        'Account',
        'Transaction',
        'AuthService',
        'LogService',
        'APP_CONSTANTS',
        function ($scope, $q, Account, Transaction, AuthService, LogService, APP_CONSTANTS) {
            var ctx;

            // Handles pagination results
            function paginationHandler(result) {
                var account = ctx.selectedAccount;
                account.pagination =  {
                    context: result.context,
                    currentPage: (result.context.offset/10) + 1,
                    totalPages: Math.ceil(result.context.totalCount/10),
                    pageToShow: result.resource
                }

                // Calculating what page numbers to show in pagination bar
                var showOrigin = account.pagination.currentPage < 4,
                    minPage = showOrigin ? 1 : (account.pagination.currentPage - 2),
                    pagesToAdd = showOrigin ? (5 - account.pagination.currentPage) : 2,
                    maxPage = account.pagination.currentPage < (account.pagination.totalPages - pagesToAdd)
                        ? (account.pagination.currentPage  + pagesToAdd)
                        : account.pagination.totalPages;
                account.pagination.pages = _.range(minPage, maxPage + 1);

            }

            function queryTransactions(account, offset) {
                $scope.loadingPages = true;
                Transaction.query({
                    accountId: account.id,
                    maxResults: 10,
                    offset: offset || 0
                }).then(function success(result) {
                    result.resource.forEach(function (transaction) {
                        transaction.account = account;
                    });
                    return result;
                }).then(paginationHandler)
                .finally(function () {
                    $scope.loadingPages = false;
                });
            }

            function init() {
                ctx = $scope.ctx = {
                    selectedAccount: undefined
                }
                AuthService.refreshUser();
            }

            $scope.transact = function (withdraw) {
                ctx.selectedAccount.transact(withdraw).then(function success() {
                    LogService.success('customer.operationSucces');
                });
            }

            $scope.paginate = function(forward) {
                return Transaction[forward ? 'queryNext' : 'queryPrevious'](ctx.selectedAccount.pagination.context).then(paginationHandler);
            }

            $scope.gotoPage = function (page) {
                return queryTransactions(ctx.selectedAccount, (page -1) *10);
            }

            $scope.showTransactions = function (account) {
                queryTransactions(account);
            }

            init();
        }
    ]);
}(angular.module('eaBank.controllers')));