(function (eaBankControllers) {
    "use strict";
    eaBankControllers.controller('AdminCtrl', [
        '$scope',
        '$q',
        'User',
        'Account',
        'LogService',
        'APP_CONSTANTS',
        function ($scope, $q, User, Account, LogService, APP_CONSTANTS) {
            var context = $scope.ctx = {};
            function init() {
                context.roles = _.map(APP_CONSTANTS.ROLES, function (role) {
                    return /ROLE_(\w+)/.exec(role)[1];
                });

                context.currencies = APP_CONSTANTS.CURRENCIES;

                User.query().then(function (userList) {
                    context.users = userList;
                });
            }

            $scope.showPasswordColumn = function () {
                return _.any(context.users, function (user) {
                    return user.editing;
                });
            }

            $scope.saveUser = function (user) {
                var accountsToSave = !user.id && user.accounts;
                user[user.id ? '$update' : '$save']().then(function success () {
                    if (accountsToSave) {
                        $q.all(_.invoke(accountsToSave, '$save')).then(function (accounts) {
                            user.accounts = accounts;
                        });
                    }
                    delete user.editing;
                });
            }

            $scope.removeUser = function (userIdx) {
                context.users[userIdx].$delete().then(function () {
                    context.users.splice(userIdx, 1);
                    LogService.success('admin.messages.userDeleted');
                });
            }

            $scope.cancel = function (user, idx) {
                if (user.id) {
                    user.$clean();
                    delete user.editing;
                } else {
                    context.users.splice(idx, 1);
                }

            }

            $scope.deleteAccount = function (user, index) {
                user.accounts[index].$delete().then(function deleted() {
                    LogService.success('admin.messages.accountDeleted');
                    user.accounts.splice(index, 1);
                })
            }

            $scope.newUser = function () {
                var emptyUser = new User({
                    editing: true,
                    role: 'ROLE_CUSTOMER',
                    accounts: []
                });
                emptyUser.accounts.push(new Account ({ user: emptyUser }));
                context.users.unshift(emptyUser);
            }

            $scope.saveAccount = function (account) {
                account[account.id ? '$update' : '$save']().then(function () {
                    LogService.success('admin.accountSaved');
                });
            }

            $scope.newAccount = function (user) {
                user.accounts = user.accounts || [];
                user.accounts.push(new Account ({ user: user }));
            }

            init();
        }
    ]);
}(angular.module('eaBank.controllers')));