(function () {
    'use strict';
    angular.module('eaBank', [
        'eaBank.configuration',
        'eaBank.services',
        'eaBank.interceptors',
        'eaBank.controllers',
        'eaBank.directives',
        'ui.bootstrap.dropdown',
        'ui.router',
        'ui.bootstrap.alert',
        'ngAnimate'
    ])
    .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('AuthHeadersInterceptor');
        }
    ])
    .run([
        '$rootScope',
        '$state',
        'I18nService',
        'AuthService',
        function ($rootScope, $state, I18nService, AuthService) {
            function init () {
                $rootScope.interpolate = angular.bind(I18nService, I18nService.getValue);

                 $rootScope.logout = function() {
                    AuthService.logout();
                    $state.go('login');
                }

                $rootScope.$on('$stateChangeStart', function (event, next, current) {
                    if (next.name !== 'login') {
                        AuthService.isLoggedIn().catch(function () {
                            $state.transitionTo('login');
                        });
                    }
                });
            }

            init();
        }
    ]);
})();
