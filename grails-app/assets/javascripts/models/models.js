(function () {
'use string'
angular.module('eaBank.models', ['awesomeResources', 'eaBank.configuration'])
    .factory('User', ['$resource', 'API_URLS', function ($resource, API_URLS) {
        return $resource({
            url: API_URLS.USER + '/:id',
            paramDefaults: { id: '@id'},
            methods: {
                getRole: function () {
                    return /ROLE_(\w+)/.exec(this.role)[1].toLowerCase();
                },
                isCustomer: function () {
                    return this.getRole() === 'customer';
                }
            },
            fields: {
                username: {
                    validators: ['notNull', 'noSpace']
                },
                name: {
                    validators: ['notNull']
                },
                surname: {
                    validators: ['notNull']
                },
                password: {
                    loader: function (pass) {
                        // If a pass is entered in the textfield, but then deleted
                        // ng-model Controller will assing an empty string. Kill it!.
                        return pass || undefined;
                    },
                    validators: [ function (pass) {
                        // Enforcing notNull but only if the user is being created
                            if (!this.id) {
                                return !(pass === undefined
                                        || pass === null
                                        || (typeof pass === 'string' && pass.length === 0)
                                    )
                                    || 'validation.notNull';
                            }
                            return true;
                        },
                        {minLength: [6, 'validation.passwordLength']},
                        {matches: [/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(.*)/, 'validation.passwordContent']}
                    ]
                },
                accounts: {
                    resource: 'Account',
                    isArray: true,
                    avoidDump: true
                },
                role: {
                    loader: function (role) {
                        if (!/ROLE_(\w+)/.test(role)) {
                            role = 'ROLE_' + role;
                        }
                        if (!this.isCustomer()) {
                            delete this.accounts;
                        }
                        return role;
                    }
                }
            }
        });
    }])
    .factory('Account', [
        '$resource',
        'Transaction',
        'APP_CONSTANTS',
        'API_URLS',
        function ($resource, Transaction, APP_CONSTANTS, API_URLS) {
            return $resource({
                url: API_URLS.ACCOUNT + '/:id',
                paramDefaults: { id: '@id'},
                classProperties: {
                    currencies: APP_CONSTANTS.CURRENCIES
                },
                init: function () {
                    // Defaulting to GBP by default
                    this.currency = this.currency || this.$static.$currencies[0];
                    this.balance = this.balance || 0;
                    this.overdraft = this.overdraft || false;
                },
                fields: {
                    deltaAmount: {
                        validators: ['positiveInteger'],
                        loader: function (number) {
                            return parseInt(number, 10);
                        }
                    },
                    balance: {
                        loader: function (balance) {
                            return parseInt(balance, 10);
                        }
                    }
                },
                methods: {
                    transact: function (withdraw) {
                        var self = this;
                        return new Transaction({
                            delta: this.deltaAmount,
                            type: withdraw ? 'WITHDRAW' : 'DEPOSIT',
                            accountId: this.id,
                            account: {
                                id: this.id
                            }
                        }).$save().then(function (transaction) {
                            self.balance += withdraw ? (-1 * self.deltaAmount) : self.deltaAmount;
                            delete self.deltaAmount;
                            return transaction;
                        });
                    }
                },
                resourceDumper: function () {
                    return {
                        overdraft: this.overdraft,
                        balance: this.balance,
                        user: {id : this.user.id },
                        currency: this.currency.name.toUpperCase()
                    }
                }
            });
        }
    ])
     .factory('Transaction', [
        '$resource',
        'APP_CONSTANTS',
        'API_URLS',
        function ($resource, APP_CONSTANTS, API_URLS) {
            return $resource({
                url: API_URLS.ACCOUNT_TRANSACTIONS + '/:id',
                paramDefaults: { accountId: '@accountId', id: '@id'},
                fields: {
                    dateCreated: {
                        loader: function (stringDate) {
                            return new Date(stringDate);
                        },
                        avoidDump: true // Set in the server
                    },
                    delta: true,
                    type: true,
                    account: true
                },
                methods: {
                    getDate: function () {
                        return this.dateCreated.toLocaleDateString() + "@" + this.dateCreated.toLocaleTimeString();
                    }
                }
            });
        }
    ]);
}());