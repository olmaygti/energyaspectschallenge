(function () {
    "use strict";
    angular.module('eaBank.directives', [])
        .directive('eaValidate', ['$rootScope', '$compile', function ($rootScope, $compile) {
            return {
                restrict: 'A',
                require: 'ngModel',
                scope: true,
                link: function (scope, elm, attrs, ngModelCtr) {
                    var render = 'after',
                        model = scope.model = scope.$eval(attrs.eaValidate),
                        template = " \
                            <div class='error' data-ng-show='model.$errors[fieldName]'> \
                                <small data-ng-repeat='error in model.$errors[fieldName]' class='error' > \
                                    {{interpolate(error, eaValidate)}} \
                                    <br/> \
                                </small> \
                            </div> \
                        ";

                    function init () {
                        if (!model || !model.$updateOrDie) { // hallmark from our resources
                            console.error('ea-validate: Must provide a valid resource instance!.');
                            return;
                        }
                        scope.fieldName = ngModelCtr.$name;
                        if (!scope.fieldName) {
                            console.error('ea-validate: Must use the "name" attribute to identify the tracked field');
                            return;
                        }
                        scope.interpolate = $rootScope.interpolate;
                        elm[render]($compile(template)(scope));

                        ngModelCtr.$viewChangeListeners.unshift(function listener () {
                            scope.invalid = !model.$validate();
                        });
                    }

                    init();
                }
            };
        }]);
})();