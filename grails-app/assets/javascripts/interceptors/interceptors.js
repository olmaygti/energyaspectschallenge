(function () {
"use strict";

    angular.module('eaBank.interceptors', ['eaBank.configuration'])
        .factory('AuthHeadersInterceptor',[
            '$rootScope',
            '$q',
            '$injector',
            'authHeaders',
            function($rootScope, $q, $injector, authHeaders){
                return {
                    request: function(config) {
                        config = config || {};

                        if (!_(authHeaders).isEmpty()) {
                            config.headers = config.headers || {};
                            _.extend(config.headers, authHeaders);
                        }
                        return config;
                    },

                    responseError: function(rejection) {
                        if(rejection.status && rejection.status === 401 || rejection.status === 403){
                            var $state = $injector.get('$state');
                            $rootScope.logout();
                            $state.go('login');
                        }
                        return $q.reject(rejection);
                    }
                }
            }
        ]);
})();