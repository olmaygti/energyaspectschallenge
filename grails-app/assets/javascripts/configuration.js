(function () {
    "user strict";

    angular.module("eaBank.configuration", [])
        .value('authHeaders', {})
        .constant('APP_CONSTANTS', {
            ROLES: ['ROLE_ADMIN', 'ROLE_CUSTOMER'],
            CURRENCIES: [
                {name: 'Pounds', symbol: 'gbp'},
                {name: 'Euros', symbol: 'eur'},
                {name: 'Dollars', symbol: 'usd'}
            ]
        })
        .constant('APP_URLS', {
            LOGIN: '/login',
            ADMIN: '/admin',
            CUSTOMER: '/customer'
        })
        .constant('TEMPLATE_URLS', {
            LOGIN: 'templates/login.html',
            ADMIN: 'templates/admin/admin.html',
            CUSTOMER: 'templates/customer/customer.html'
        })
        .constant('API_URLS', {
            LOGIN_CHALLENGE: 'api/login',
            USER: 'api/user',
            ACCOUNT: 'api/account',
            ACCOUNT_TRANSACTIONS: 'api/account/:accountId/transaction'
        });
})();