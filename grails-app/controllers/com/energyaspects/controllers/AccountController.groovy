package com.energyaspects.controllers

import com.energyaspects.domain.Account
import com.energyaspects.domain.User
import grails.rest.RestfulController

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_CUSTOMER'])
class AccountController extends RestfulController {

    static responseFormats = ['json']

    AccountController() {
        super(Account)
    }
}