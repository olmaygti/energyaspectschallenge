package com.energyaspects.controllers

import com.energyaspects.domain.Transaction
import com.energyaspects.domain.TransactionType
import com.energyaspects.domain.Account

import grails.rest.RestfulController
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN', 'ROLE_CUSTOMER'])
class TransactionController extends RestfulController {

    static responseFormats = ["json"]

    TransactionController() {
        super(Transaction)
    }

    def index () {
        def items = Transaction.createCriteria().list(max: params.maxResults ?: 10, offset: params.offset ?: 0) {
            "account" {
                eq('id', params.accountId as long)
            }

            order('dateCreated', 'desc')
        }

        respond([
            items: items,
            totalCount:  items.totalCount
        ])
    }
}