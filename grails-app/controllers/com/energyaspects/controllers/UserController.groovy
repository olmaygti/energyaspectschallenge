package com.energyaspects.controllers

import com.energyaspects.domain.*

import grails.rest.RestfulController
import grails.plugin.springsecurity.annotation.Secured
import grails.converters.JSON
import org.springframework.transaction.annotation.*


@Secured(['ROLE_ADMIN'])
class UserController extends RestfulController {

    static responseFormats = ['json']

    UserController() {
        super(User)
    }

    @Transactional
    def save() {
        def user = new User(request.JSON).save(flush:true)
        if(user) {
            def role =  Role.findByAuthority(request.JSON.role)
            UserRole.create(user, role, true)
            render user.toMap() as JSON
        } else {
            render status:400
        }
    }

    @Transactional
    def update (User user) {
        respond user.save(flush:true)?.toMap()

    }

    @Transactional
    def delete(User user) {
        Account.findAllByUser(user).each { account ->
            Transaction.where {
                account == account
            }.deleteAll()
            account.delete()
        }

        user.authorities.each { auth ->
            UserRole.remove(user, auth, true)
        }
        user.delete(flush:true)
        render status:200
    }


    @Secured(['ROLE_ADMIN', 'ROLE_CUSTOMER'])
    def index () {
        if (params.username) {
            render User.withCriteria {
                eq('username', params.username)
            }*.toMap() as JSON
        } else {
            render User.list()*.toMap() as JSON
        }
    }
}

