package com.energyaspects.domain

class User {

    transient springSecurityService

    String name
    String surname

    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static transients = ['springSecurityService']

    static constraints = {
        username blank: false, unique: true
        password blank: false
    }

    static mapping = {
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }

    def toMap() {
        [
            id: this.id,
            accountExpired: this.accountExpired,
            accountLocked: this.accountLocked,
            enabled: this.enabled,
            name: this.name,
            surname: this.surname,
            username: this.username,
            // This demo app assumes just one role per user 
            role: this.authorities[0].authority,
            accounts: Account.findAllByUser(this)
        ]
    }

    @Override
    public String toString() {
        "$name $surname"
    }
}
