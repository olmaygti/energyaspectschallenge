package com.energyaspects.domain

class Account {


    String iban

    Long balance

    Currency currency

    Boolean overdraft

    List<Transaction> transactions

    static belongsTo = [user: User]

    static hasMany = [transactions: Transaction]

    static String generateRandomUUID() {
        (UUID.randomUUID().toString() + UUID.randomUUID().toString()).replaceAll('-', '')
    }

    def beforeValidate() {
        if(!this.iban) {
            this.iban = generateRandomUUID()
        }
    }
}

enum Currency {
    POUNDS('Pounds', 'gbp'),
    DOLLARS('Dollars', 'usd'),
    EUROS('Euros', 'eur')

    String symbol
    String name

    private Currency(String name, String symbol) {
        this.name = name
        this.symbol = symbol
    }
}