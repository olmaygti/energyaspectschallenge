package com.energyaspects.domain

class Transaction {

    Long delta

    Date dateCreated

    TransactionType type

    static belongsTo = [account: Account]

    def beforeInsert () {
        this.account.balance += this.type == TransactionType.DEPOSIT ? this.delta : (-1 * this.delta)
    }
}

enum TransactionType {
    WITHDRAW("Withdraw"),
    DEPOSIT("Deposit"),
    TRANSFER("Transfer")

    String type

    private TransactionType(String type) {
        this.type = type
    }
}