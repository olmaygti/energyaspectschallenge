import com.energyaspects.domain.*

import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def customerRole = new Role(authority: 'ROLE_CUSTOMER').save(flush: true)

        def admin = new User(name: 'Bernard', surname:'Madoff', username: 'admin', password: 'admin').save(flush: true)

        UserRole.create(admin, adminRole, true)

        def me = new User(name: 'Amo', surname:'Chohan', username: 'amo', password: 'amo').save(flush: true)

        UserRole.create(me, customerRole, true)

        def myAccount = new Account(balance: 0, currency: Currency.POUNDS, overdraft: false, user: me).save(flush:true)
        def mySecondAccount = new Account(balance: 0, currency: Currency.EUROS, overdraft: true, user: me).save(flush:true)

        100.times {
            myAccount.addToTransactions(new Transaction(account: myAccount, delta: 1, type: TransactionType.DEPOSIT).save(flush:true))
            mySecondAccount.addToTransactions(new Transaction(account: mySecondAccount, delta: 1, type: TransactionType.DEPOSIT).save(flush:true))
        }

        registerMarshallers()
    }
    def destroy = {
    }

    def registerMarshallers() {
        JSON.registerObjectMarshaller(Account) { account ->
            [
                id: account.id,
                iban: account.iban,
                overdraft: account.overdraft,
                balance: account.balance,
                user: [id: account.user.id],
                currency: [
                    name: account.currency.name,
                    symbol: account.currency.symbol
                ]
            ]
        }
    }
}
