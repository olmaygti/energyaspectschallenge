class UrlMappings {

	static mappings = {
        "/api/user" resources: 'user'
        "/api/account" (resources: 'account') {
            "/transaction" resources: 'transaction'
        }

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
