<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
	</head>
	<body>
		<div class="container" data-ng-app="eaBank" style="position:relative;">
            <div class="container alerts" style="">
                <alert class="animated-fade"
                    data-ng-repeat="alert in alerts"
                    type="{{ alert.type }}"
                    close="closeAlert($index)">
                    <div>
                        <strong>
                            {{alert.msg}}
                        </strong>
                    </div>
                </alert>
            </div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand">
                        {{ ::interpolate('global.header')}}
                    </a>
                </div>
                <ul data-ng-if="user" class="nav navbar-nav navbar-right">
                    <li dropdown>
                        <a href="#" class="dropdown-toggle" dropdown-toggle>
                            Welcome {{ user.name }} {{ user.surname }}!
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-ng-click="logout()">{{ ::interpolate('global.logout') }}</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <hr>
	    	<div data-ui-view></div>
    	</div> <!-- /container -->
	</body>
</html>
